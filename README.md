# GoPress

## Dev Scripts
All build-system scripts are piped through npm:
`npm run {script-name}`
### Front-End
**For Local Testing/Development**
`npm run ui-dev`

**For Production**
`npm run ui-prod`
### Back-End
**For Local Testing/Development**
`npm run server-dev`

**For Production**
`npm run server-prod`

## UI Quick Start
Vuetify has already been set up to make building UIs a little easier.
Check out the docs here: https://vuetifyjs.com/en/components/navigation-drawers

See `web/src/views/Home.vue` for an existing example.