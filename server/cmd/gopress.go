package main

import (
	"fmt"
	"gitlab.com/jasonbdly/gopress/server/internal/app"
	"log"
)

/*func httplog(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[%s]%s: %s\n", time.Now().Format(time.RFC3339), r.Method, r.URL)
		h.ServeHTTP(w, r)
	})
}*/

func main() {
	a := app.App{}
	a.Start()

	/*router := mux.NewRouter()

	// Gotta be a way to reduce these three into one handler, need to look into this further
	router.Handle("/", httplog(http.FileServer(http.Dir("./../../web/static"))))
	router.PathPrefix("/js").Handler(httplog(http.FileServer(http.Dir("./../../web/static"))))
	router.PathPrefix("/css/").Handler(httplog(http.FileServer(http.Dir("./../../web/static"))))

	server := &http.Server{
		Handler:      router,
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	fmt.Println("Server Online")

	log.Fatal(server.ListenAndServe())*/
}
