package app

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type App struct {
	Router *mux.Router
	config Config
}

var defaultConfigPath string = "./config.json"

func (a *App) Start() {
	a.StartWithConfig(ConfigFromFile(defaultConfigPath))
}

func (a *App) StartWithConfig(config Config) {
	a.config = config

	a.Router = mux.NewRouter()

	//a.initializeRoutes()

	log.Fatal(http.ListenAndServe(a.config.Host+":"+a.config.Port, a.Router))
}

func (a *App) initializeRoutes() {
	fmt.Println("Initializing Routes")
	for _, config := range a.config.Modules {

	}

	/*for _, handler := range a.endpoints {
		fmt.Printf("Registering Handler for Path: %s\n", handler.Path)
		a.Router.HandleFunc(string(handler.Path), handler.Handler).Methods(string(handler.Method))
	}*/
}
