package app

import (
	"encoding/json"
	"gitlab.com/jasonbdly/gopress/server/internal/module"
	"ioutil"
)

func ConfigFromFile(path string) (config Config, err error) {
	configBytes, err := ioutil.ReadAll(path)

	if err != nil {
		return
	}

	err = json.Unmarshal(configBytes, &config)

	if err != nil {
		return
	}
}

type Config struct {
	Host    string          `json:"host"`
	Port    string          `json:"port"`
	Modules []module.Config `json:"modules"`
}

var defaultModules []string = []string{
	"../modules/user/user",
}

func getDefaultConfig() Config {
	cfg := Config{
		Host: "localhost",
		Port: "8080",
	}

	moduleConfigs := []module.Config{}
	for _, modulePath := range defaultModules {
		moduleConfig, err := module.ConfigFromFile(modulePath)
		if err != nil {
			log.Fatal(err)
		}
		moduleConfigs = append(moduleConfigs, moduleConfig)
	}

	cfg.Modules = moduleConfigs

	return cfg
}
