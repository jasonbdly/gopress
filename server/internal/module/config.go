package module

import (
	"encoding/json"
	"gitlab.com/jasonbdly/gopress/server/internal/app"
	"ioutil"
)

func ConfigFromFile(path string) (config Config, err error) {
	configBytes, err := ioutil.ReadAll(path)

	if err != nil {
		return
	}

	err = json.Unmarshal(configBytes, &config)

	if err != nil {
		return
	}
}

type Config struct {
	Port string `json:"port"`
	Host string `json:"host"`
	Path string `json:"path"`
}
