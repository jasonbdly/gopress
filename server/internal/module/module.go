package module

import (
	"gitlab.com/jasonbdly/gopress/server/internal/app"
	context "golang.org/x/net/context"
	"net/http"
)

// Modules should implement these methods
type Module interface {
	HandleGet(*Request) (*Response, error)
	HandlePost(*Request) (*Response, error)
	HandlePut(*Request) (*Response, error)
	HandleDelete(*Request) (*Response, error)
}

// RPC unwrapper for module server-side
type ModuleServer struct {
	module Module
}

func NewModuleServer(module Module) *ModuleServer {
	return &ModuleServer{
		module: module,
	}
}

func (s *ModuleServer) Start() {
	// Parse local module config
	// Start HTTP Server on configured port

}

func (s *ModuleServer) HandleGet(context.Context, *Request) (*Response, error) {
	return nil, nil
}

func (s *ModuleServer) HandlePost(context.Context, *Request) (*Response, error) {
	return nil, nil
}

func (s *ModuleServer) HandlePut(context.Context, *Request) (*Response, error) {
	return nil, nil
}

func (s *ModuleServer) HandleDelete(context.Context, *Request) (*Response, error) {
	return nil, nil
}
