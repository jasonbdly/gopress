package main

import (
	"gitlab.com/jasonbdly/gopress/server/internal/module"
)

type UserModule struct {
}

func (u *UserModule) HandleGet(request *module.Request) (*module.Response, error) {
	return nil, nil
}

func (u *UserModule) HandlePost(request *module.Request) (*module.Response, error) {
	return nil, nil
}

func (u *UserModule) HandlePut(request *module.Request) (*module.Response, error) {
	return nil, nil
}

func (u *UserModule) HandleDelete(request *module.Request) (*module.Response, error) {
	return nil, nil
}
